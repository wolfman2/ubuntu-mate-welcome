#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-03-10 10:57+0000\n"
"PO-Revision-Date: 2016-03-15 17:14+0100\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language-Team: : LANGUAGE\n"
"Report-Msgid-Bugs-To: you@example.com\n"
"X-Generator: Poedit 1.8.7.1\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en_GB\n"

#: splash.html:18
msgid "Welcome"
msgstr "Willkommen"
