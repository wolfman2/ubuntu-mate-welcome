#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: : LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: community.html:17
msgid "Community"
msgstr ""

#: community.html:25
msgid "Forum"
msgstr ""

#: community.html:26
msgid ""
"If you want to chat with the Ubuntu MATE team and other members of our "
"growing community, this is where to go. The best place to play an active "
"role in the Ubuntu MATE community and to help shape its direction is via our"
" forum."
msgstr ""

#: community.html:31
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr ""

#: community.html:32
msgid "Sorry, Welcome was unable to establish a connection."
msgstr ""

#: community.html:33
msgid "Retry"
msgstr ""

#: community.html:38
msgid "Social Networks"
msgstr ""

#: community.html:39
msgid "Ubuntu MATE is active on the following social networks."
msgstr ""
